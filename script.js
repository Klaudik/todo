let todoForm;
let todoFormStyle;
let todoList;
let showHideButton;
let checkbox;
let label;
let select;
let options;
let selected = "Main List";
let rating;
let newList;
let taskList;
let taskBar;
let newListButton;
let newListInput;
let newTaskInput;
let group;
let listOfGroups = [];
let height = 33;
let newGroup;
let starValue;
let statementForGroup;
document.addEventListener('DOMContentLoaded', function()
{
    taskBar = [];
    todoForm = document.querySelector('#todoForm');
    todoList = document.querySelector('#todoList');
    showHideButton = document.querySelector("#showAndHideButton");
    rating = document.querySelector('#rating');
    checkbox = rating.querySelectorAll('input');
    label = document.getElementsByClassName("star");
    select = document.querySelector("select");
    options = select.getElementsByTagName("option");
    newList = document.querySelector('#newList');
    tasks = document.querySelectorAll('task');
    newListButton = newList.querySelector('button');
    newListInput = newList.querySelector('input');
    newTaskInput = todoForm.querySelector('#newTaskInput');
    newTaskInput.style.display = "flex";
    todoForm.style.setProperty("height", "0px");
    let statement = "white";
    
    listOfGroups = [new Group("Main List", 0)];
    listOfGroups[0].addList();
    options = [];

todoList.addEventListener("click", e => {
    let arrow = e.target.querySelector("button"); 
    if(arrow.style.getPropertyValue("transform") == "rotate(-180deg)"){
        arrow.style.setProperty("transform", "rotate(0deg)");
        arrow.style.setProperty("transition", "0.5s");
        e.target.nextElementSibling.style.setProperty("height", "0px");
    }
    else{
        for(let i = 0; i <= listOfGroups.length-1; i++){
            if(listOfGroups[i].name == e.target.id){
                arrow.style.setProperty("transform", "rotate(-180deg)");
                arrow.style.setProperty("transition", "0.5s");
                e.target.nextElementSibling.style.setProperty("height", listOfGroups[i].height + "px");
            }
        }   
    }
});
        todoForm.addEventListener("submit", function(e){
            e.preventDefault();
            let textarea = todoForm.querySelector('textarea');
            for(let i = 0; i < listOfGroups.length; i++){
                if(listOfGroups[i].name == selected){
                    if(textarea.value !== ''){
                        listOfGroups[i].addTask(textarea.value, starValue, statementForGroup);
                        listOfGroups[i].changeHeight(true);
                        textarea.value='';
                    };
                }
            };
        });
        todoList.addEventListener("click", function(e){
            if(e.target.classList.contains(".deleteButton") !== null){
                let groupId = e.target.parentNode.parentNode;
                let obj = listOfGroups.find(x => x.name == groupId.parentNode.id);
                let objTask = obj.tasksTab.indexOf(obj.tasksTab.find(x=> x == e.target.parentNode.id));
                e.target.closest(".task").remove();
                obj.tasksTab.splice(1,objTask);
                obj.changeHeight(false);
            }
        });
        showHideButton.addEventListener("click", function(){
            if(todoForm.style.height === "0px"){
                showHideButton.classList.remove("icon-plus");
                showHideButton.classList.add("icon-minus");
                todoForm.style.setProperty("height", "150px");
            }
            else{
                showHideButton.classList.remove("icon-minus");
                showHideButton.classList.add("icon-plus");
                todoForm.style.setProperty("height", "0px");
            }
        }); 
        select.addEventListener("change", function(){
                if(select.value == "new"){
                    newList.style.setProperty("display", "flex");
                    newTaskInput.style.setProperty("display", "none");
                }
                else{
                    newList.style.setProperty("display", "none");
                    newTaskInput.style.setProperty("display", "flex");
                }
                selected = select.value;
                return selected;  
        });
        newListButton.addEventListener("click", function(){
            let newG = new Group(newListInput.value,0);
            newG.addList();
            listOfGroups.push(newG);
            taskBar = document.getElementsByClassName("taskBar");
            addOption(newListInput.value);
            newListInput.value = '';
        });
        
        for(i = 0; i < checkbox.length; i++){
            checkbox[i].addEventListener("click", function(e){
                for(let j = 0 ; j < checkbox.length; j++){
                    if(j<this.value){
                        label[j].style.setProperty("--color", "lightgrey");
                    };
                    if(j >= this.value-1){
                        label[j].style.setProperty("--color", "blue");
                    };
                    if(this.value == 0){
                        if(statement == "lightgrey"){
                            label[4].style.setProperty("--color", "blue");
                            starValue = 0;
                            
                        };
                        if(statement == "blue"){
                            label[4].style.setProperty("--color", "lightgrey");
                            starValue = 1;
                            
                        };
                        for(let k = 0; k < 4; k++){
                            label[k].style.setProperty("--color", "lightgrey");
                        };
                    };
                    starValue = e.target.id;
                    statement = label[4].style.getPropertyValue("--color");
                    statementForGroup = statement;
                };
            });
        };
});
function addOption(which){
    let newOption = document.createElement('option');
    newOption.value = which;
    newOption.innerText = which;
    select.appendChild(newOption);
    options.push(newOption);
};
class Group {
    newGroup = null;
    tasksTab = [];
    constructor(name, height){
        this.name = name;
        this.height = height;
    }
    addList(){
        let newListTask = document.createElement('div');
        let newListBar = document.createElement('div');
        newListBar.classList.add("taskBar");
        newListBar.id = this.name;
    
        let newMainTitle = document.createElement('div');
        newMainTitle.classList.add("mainTitle");
        newMainTitle.innerText = this.name;
    
        let newtaksButton = document.createElement('button');
        newtaksButton.classList.add("icon-down-dir");
    
        this.newGroup = document.createElement('div');
        this.newGroup.classList.add("group");
        this.newGroup.setAttribute('id', this.name);
    
        newListBar.appendChild(newMainTitle);
        newListBar.appendChild(newtaksButton);
                
        newListTask.appendChild(newListBar);
        newListTask.appendChild(this.newGroup);
        todoList.appendChild(newListTask);
        return newGroup;
    }
    addTask(text, value, statement){
        let randomId = this.getRandom();
        if(value == "5" && statement == "blue"){
            value = "0";
        }
        if(value == "5" && statement == "lightgrey"){
            value = "-1";
        }
        let todo = document.createElement('div');
        todo.classList.add('task');
        todo.setAttribute('id', randomId);
    
        let todoTittle = document.createElement('div');
        todoTittle.classList.add('tittle');
        todoTittle.innerText = text;

        let taskStars = document.createElement('div');
        taskStars.classList.add('taskStars');

        for(let i = 0; i < 5; i++){
            let star = document.createElement('i');
            star.classList.add('icon-star');
            taskStars.appendChild(star);
            switch(value) {
                case "5":
                    if(statement == "blue"){
                        if(i == 0){
                            star.style.color = "blue";
                        }
                        else{
                            star.style.color = "white";
                        }
                    }
                    if(statement == "lightgrey"){
                        star.style.color = "white";
                    }
                    break;
                
                default:
                    if(value >= i){
                        star.style.color = "blue";
                    }
                    else{
                        star.style.color = "white";
                    }
            };
        };
        
        let todoDeleteButton = document.createElement('div');
        todoDeleteButton.classList.add('deleteButton');
    
        let todoButton = document.createElement('button');
        todoButton.classList.add('buttonik');
    
        let todoIcon = document.createElement('i');
        todoIcon.classList.add('icon-cancel');
        todoButton.appendChild(todoIcon);
        todoDeleteButton.appendChild(todoButton);
    
        todo.appendChild(todoTittle);
        todo.appendChild(taskStars);
        todo.appendChild(todoDeleteButton);
        
        if(this.tasksTab.length == 0){
            let newTask = new Task(todo,value,randomId);
            this.tasksTab.push(newTask);
            this.newGroup.appendChild(todo);
            this.tasksTab.sort(function(a,b){
                return b.stars - a.stars;
            });
            return;
        }
        
        else{
            let smaller;
            for(let z = 0; z < this.tasksTab.length; z++){
                if(value == 5){
                    let number = (statement=="blue") ? "0" : "-1";
                    let newTask = new Task(todo,number,randomId);
                    this.tasksTab.push(newTask);
                    switch (number){
                        case "-1":
                            this.newGroup.appendChild(todo);
                            this.tasksTab.sort(function(a,b){
                                return b.stars - a.stars;
                            });
                            value = 1;
                            break;
                        case "0":
                            value = 0;
                            break;
                    }
                    console.log(value);
                }
                if(value > this.tasksTab[z].stars){
                    let newTask = new Task(todo,value,randomId);
                    this.tasksTab.push(newTask);
                    todo = this.newGroup.insertBefore( todo, this.tasksTab[z].element);
                    this.tasksTab.sort(function(a,b){
                        return b.stars - a.stars;
                    });
                    return;
                }
                if(value == this.tasksTab[z].stars){
                    let newTask = new Task(todo,value,randomId);
                    this.tasksTab.push(newTask);
                    todo = this.newGroup.insertBefore( todo, this.tasksTab[z].element.nextElementSibling);
                    this.tasksTab.sort(function(a,b){
                        return b.stars - a.stars;
                    });
                    return;
                }
                if(value < this.tasksTab[z].stars){
                    smaller = this.tasksTab[z];
                    if(this.tasksTab[z] == this.tasksTab.length){
                        let newTask = new Task(todo,value,randomId);
                        this.tasksTab.push(newTask);
                        todo = this.newGroup.insertBefore( todo, smaller.element.nextElementSibling);
                        this.tasksTab.sort(function(a,b){
                            return b.stars - a.stars;
                        });
                        return;
                    }
                } 
            }
            let newTask = new Task(todo,value,randomId);
                this.tasksTab.push(newTask);
                todo = this.newGroup.insertBefore( todo, smaller.element.nextElementSibling);
                this.tasksTab.sort(function(a,b){
                    return b.stars - a.stars;
                });
        };  
    };
    getRandom(){
        let min = Math.ceil(100);
        let max = Math.floor(999);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    changeHeight(which){
        if(which){
            this.height += 33;
            this.newGroup.style.setProperty("height", this.height);
        }
        if(!which){
            this.height -= 33;
            this.newGroup.style.setProperty("height", this.height);
        }
    };
};
class Task{
    constructor(element, stars, id){
        this.element = element;
        this.stars = stars;
        this.id = id;
    }
};   